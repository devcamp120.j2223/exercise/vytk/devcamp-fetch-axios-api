import FetchApi from "./components/FetchApi";
import 'bootstrap/dist/css/bootstrap.min.css'
import AxiosLibary from "./components/AxiosApi";
function App() {
  return (
    <div className="container mt-5">
      <FetchApi/>
      <AxiosLibary/>
    </div>
  );
}

export default App;
