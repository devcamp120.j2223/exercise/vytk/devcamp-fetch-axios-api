import { Component } from "react";
const BaseURL='https://jsonplaceholder.typicode.com/posts';
class FetchApi extends Component{
    fetchAPI=async(url,body)=>{
        let res=await fetch(url,body);
        let data=await res.json();
        return data;
    }
    getAllAPI=()=>{
        // fetch(BaseURL)
        // .then((response)=>response.json)
        // .then((json)=>{
        //     console.log(json);
        // })
        this.fetchAPI(BaseURL)
        .then((data)=>{
            console.log(data);
        })
    }
    getAPIByID=()=>{
        this.fetchAPI(BaseURL+'/1')
        .then((data)=>{
            console.log(data);
        })
    }
    postAPI=()=>{
        let body={
            method: 'POST',
            body: JSON.stringify({
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchAPI(BaseURL,body)
        .then((data)=>{
            console.log(data);
        })
    }
    updateAPI=()=>{
        let body={
            method: 'PUT',
            body: JSON.stringify({
              id: 1,
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.fetchAPI(BaseURL+'/1',body)
        .then((data)=>{
            console.log(data);
        })
    }
    deleteAPI=()=>{
        let body={
            method: 'DELETE',
        }
        this.fetchAPI(BaseURL+'/1',body)
        .then((data)=>{
            console.log(data);
        })
    }
    render(){
        return(
            <div className="col-sm-12">
                <h5 className="text-center text-success mb-3">Fetch Api</h5>
                <div className="row">
                    <div className="col-sm"><button className="btn btn-success" onClick={this.getAllAPI}>GET All API</button></div>
                    <div className="col-sm"><button className="btn btn-success" onClick={this.getAPIByID}>GET API By ID</button></div>
                    <div className="col-sm"><button className="btn btn-success" onClick={this.postAPI}>POST API</button></div>
                    <div className="col-sm"><button className="btn btn-success" onClick={this.updateAPI}>UPDATE API By ID</button></div>
                    <div className="col-sm"><button className="btn btn-success" onClick={this.deleteAPI}>DELETE API By ID</button></div>
                </div>
            </div>
        )
    }
}

export default FetchApi;