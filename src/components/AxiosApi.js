import { Component } from "react";
import axios from "axios";
const BaseURL='https://jsonplaceholder.typicode.com/posts';
class AxiosLibary extends Component{
    axiosLibary=async(url,body)=>{
        let res=await axios(url,body);
        return res.data;
    }
    getAllAPI=()=>{
        this.axiosLibary(BaseURL)
        .then((data)=>{
            console.log(data);
        })
    }
    getAPIByID=()=>{
        this.axiosLibary(BaseURL+'/1')
        .then((data)=>{
            console.log(data);
        })
    }
    postAPI=()=>{
        let body={
            method: 'POST',
            body: JSON.stringify({
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosLibary(BaseURL,body)
        .then((data)=>{
            console.log(data);
        })
    }
    updateAPI=()=>{
        let body={
            method: 'PUT',
            body: JSON.stringify({
              id: 1,
              title: 'foo',
              body: 'bar',
              userId: 1,
            }),
            headers: {
              'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosLibary(BaseURL+'/1',body)
        .then((data)=>{
            console.log(data);
        })
    }
    deleteAPI=()=>{
        let body={
            method: 'DELETE',
        }
        this.axiosLibary(BaseURL+'/1',body)
        .then((data)=>{
            console.log(data);
        })
    }
    render(){
        return(
            <div className="col-sm-12 mt-5">
                <h5 className="text-center text-info mb-3">Axios Libary</h5>
                <div className="row">
                    <div className="col-sm"><button className="btn btn-info text-light" onClick={this.getAllAPI}>GET All API</button></div>
                    <div className="col-sm"><button className="btn btn-info text-light" onClick={this.getAPIByID}>GET API By ID</button></div>
                    <div className="col-sm"><button className="btn btn-info text-light" onClick={this.postAPI}>POST API</button></div>
                    <div className="col-sm"><button className="btn btn-info text-light" onClick={this.updateAPI}>UPDATE API By ID</button></div>
                    <div className="col-sm"><button className="btn btn-info text-light" onClick={this.deleteAPI}>DELETE API By ID</button></div>
                </div>
            </div>
        )
    }
}
export default AxiosLibary;